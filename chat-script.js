'use strict';

var _createClass = (function() {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ('value' in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }
  return function(Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
})();

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true });
  } else {
    obj[key] = value;
  }
  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError('Cannot call a class as a function');
  }
}

/*
  Almost all copy for the bot is pulled from here. Primary exception is for the any of the lines below that have dynamic content being pulled in. Vars were being returned as undefined, so these lines of text are applied directly in botHandler()
*/
// --------------- Script Data -----------------------
let media = {
  img: 'https://s3.amazonaws.com/XXXX/XXXXX.jpg'
};
// set script responses based on user response and status
// TODO: move this to dynamoDB
let paths = {
  /*
    pass as object of objects, key used as statusL
  
    key:{
      response1:'xxxx',
      response2:'xxxx'
    },
    i1_scene1:{
      btnOptions: [
        {
          title: 'xxxx',
          payload: 'xxxx'
        },
        {
          title: 'xxxx',
          payload: 'xxxx'
        },
        {
          title: 'xxxx',
          payload: 'xxxx'
        },
        {
          title: 'xxxx',
          payload: 'xxxx'
        }
      ]
    }
  */
};

var botScriptData = (function() {
  function botScriptData() {
    _classCallCheck(this, botScriptData);
  }
  _createClass(botScriptData, [
    {
      key: 'phase1Script',
      value: function phase1Script() {
        return paths;
      }
    }
  ]);

  return botScriptData;
})();

exports.default = botScriptData;
module.exports = exports['default'];
