'use strict';

//environment vars for setting up bot connection to FB page and database
//_config establishes server connection
let botConfig = require('./_config');
let config = new botConfig();
let envInfo = config.configInfo();

const request = require('superagent');
let FBMessenger = require('fb-messenger');
let messenger = new FBMessenger(envInfo.pageAccessToken);

/*
  chat script/copy for the bot pulled from here.
*/
let botScriptData = require('./chat-script');
let botScript = new botScriptData();

/**
 * DATABASE INIT
 * TODO: Add flag to remove db connection, not being used
 */
let dbHandler = require('./dbHandler');
let db = new dbHandler();

var msgObj = null,
  yesResponses = [
    '👍',
    '👍🏻',
    '👍🏼',
    '👍🏽',
    '👍🏾',
    '👍🏿',
    'sure',
    'yea',
    'yeah',
    'why not',
    'okay',
    'okey doke',
    'yes im doing good',
    'yes definitely',
    'yes i do',
    'yes makes sense',
    'yes i would',
    'yes 100%',
    'affirmative',
    'fine',
    'good',
    'great',
    'okay',
    'ok',
    'true',
    'yea',
    'yeah',
    'all right',
    'allright',
    'alright',
    'aye',
    'beyond a doubt',
    'certainly',
    'definitely',
    'exactly',
    'good enough',
    'gladly',
    'granted',
    'indubitably',
    'just so',
    'most assuredly',
    'naturally',
    'of course',
    'positively',
    'precisely',
    'sure',
    'sure thing',
    'surely',
    'understand',
    'understood',
    'undoubtedly',
    'unquestionably',
    'very well',
    'willingly',
    'without fail',
    'yep',
    'yes',
    'i guess',
    'i suppose',
    'makes sense',
    'cool',
    'seems legit',
    'meh',
    'sweet',
    'got it',
    'i do',
    'i can',
    'i think so',
    'y',
    'i am',
    'you bet',
    'sounds fair',
    'correct',
    'Y',
    'maybe',
    'not sure',
    'probably',
    'possibly',
    'i don’t know',
    'i did',
    'it is'
  ],
  noResponses = [
    '👎',
    '👎🏻',
    '👎🏼',
    '👎🏽',
    '👎🏾',
    '👎🏿',
    'nah',
    'uh uh',
    'dont think so',
    'no this is the worst',
    'no i want to stay',
    'no i dont',
    'no clue',
    'no thanks',
    'no',
    'nope',
    'nada',
    'declined',
    'not at all',
    'negative',
    'n',
    'N',
    'guess not'
  ];

let changeStatus = (ID, newStatus) => {
  let nxt = typeof newStatus !== 'undefined' ? newStatus : user[ID].currentPhase.paths[user[ID].status].nextStatus;

  user[ID].status = nxt;

  let obj = {
    status: user[ID].status
  };
  updateUserInfo(ID, obj, () => {
    botHandler(ID, {}, false);
  });
};

// --------------- Helpers -----------------------

/**
 * Main call to send a message to the user
 * @param  {} str Message being sent
 * @param  {} callback optional callback function
 */

let sendTextMessage = (ID, str, callback) => {
  callback = typeof callback !== 'undefined' ? callback : null;

  messenger.sendTextMessage(ID, str, function(err, body) {
    if (err) return console.error('error', err);
    if (callback !== null) {
      callback();
    }
  });
};
/**
 * Send typing dots status to user
 * @param  {} duration How long to show typing dots
 * @param  {} callback optional callback function
 */
let showTyping = (ID, duration, callback) => {
  callback = typeof callback !== 'undefined' ? callback : null;
  messenger.sendAction(ID, 'typing_on');
  setTimeout(() => {
    messenger.sendAction(ID, 'typing_off');
    callback();
  }, duration);
};
/**
 * Mark user's message as seen
 * @param  {} duration delay before sending callback after marking seen
 * @param  {} callback optional callback function
 */
let showSeen = (ID, duration, callback) => {
  callback = typeof callback !== 'undefined' ? callback : null;
  messenger.sendAction(ID, 'mark_seen');
  setTimeout(() => {
    callback();
  }, duration);
};

/**
 * Send user an image
 * @param  {} img absolute URL  to image
 * @param  {} callback optional callback function
 */
let sendImage = (ID, img, callback) => {
  callback = typeof callback !== 'undefined' ? callback : null;
  messenger.sendImageMessage(ID, img, function(err, evt) {
    if (err) return console.log(err);
    callback();
  });
};

/**
 * Send user an image
 * @param  {} img absolute URL  to image
 * @param  {} callback optional callback function
 */
let sendSavedImage = (ID, img, callback) => {
  callback = typeof callback !== 'undefined' ? callback : null;
  var messageData = {
    attachment: {
      type: 'image',
      payload: {
        attachment_id: img
      }
    }
  };

  messenger.sendMessage(ID, messageData, function(err, evt) {
    if (err) return console.log(err);
    try {
      callback();
    } catch (e) {
      console.log(e);
      /* */
    }
  });
};

/**
 * Send user an image
 * @param  {} img absolute URL  to image
 * @param  {} callback optional callback function
 */
let sendSavedVideo = (ID, vid, callback) => {
  callback = typeof callback !== 'undefined' ? callback : null;
  var messageData = {
    attachment: {
      type: 'video',
      payload: {
        attachment_id: vid
      }
    }
  };

  messenger.sendMessage(ID, messageData, function(err, evt) {
    if (err) return console.log(err);
    try {
      callback();
    } catch (e) {
      console.log(e);
      /* */
    }
  });
};

/**
 * Send user an image
 * @param  {} img absolute URL  to image
 * @param  {} callback optional callback function
 */
let sendVideo = (ID, vid, callback) => {
  callback = typeof callback !== 'undefined' ? callback : null;
  var messageData = {
    attachment: {
      type: 'video',
      payload: {
        url: vid
      }
    }
  };

  messenger.sendMessage(ID, messageData, function(err, evt) {
    if (err) return console.log(err);
    try {
      callback();
    } catch (e) {
      console.log(e);
      /* */
    }
  });
};
/**
 * Send user a message with buttons as response options
 * @param  {} str String of message/question sent to user
 * @param  {} btnOptions Structure below. title = label, payload = returned value from user response
  [
    { title: 'YES', payload: 'videoIntro'},
    { title: 'NO', payload: 'surveyStart'}
  ]
 * @param  {} callback optional callback function
 */
let sendButtonMessage = (ID, str, btnOptions, callback) => {
  callback = typeof callback !== 'undefined' ? callback : null;
  var btnArray = [];
  for (var i = 0; i < btnOptions.length; i++) {
    btnArray.push({
      content_type: 'text',
      title: btnOptions[i].title,
      payload: btnOptions[i].payload
    });
  }
  messenger.sendQuickRepliesMessage(ID, str, btnArray, function(err, evt) {
    if (err) return console.log(err);
    try {
      callback();
    } catch (e) {
      /* */
    }
  });
};
/**
 * Default message sent if reponse isn't matched
 * three attempts allowed before exit
 *
 */
let rejectStatus = '';
let rejectCount = 1;
const goodbyeResponse = 'I thought you’d be more helpful than that.\nMaybe we can try again another time.\nGood bye.';

let sendRejectionMessage = (ID, YesNoCheck) => {
  YesNoCheck = typeof YesNoCheck !== 'undefined' ? YesNoCheck : false;
  let selected = '';

  // Assign default string responses
  let responses = [
    /**/
  ];
  let yesnoResponses = [
    /**/
  ];
  if (user[ID].attempts < 1) {
    selected = responses[Math.floor(Math.random() * responses.length)];
    sendTextMessage(ID, selected);
    user[ID].attempts += 1;
  } else if (user[ID].attempts == 1) {
    selected = YesNoCheck
      ? yesnoResponses[Math.floor(Math.random() * yesnoResponses.length)]
      : responses[Math.floor(Math.random() * responses.length)];
    sendTextMessage(ID, selected);
    user[ID].attempts += 1;
  } else {
    sendTextMessage(ID, goodbyeResponse);
    // user[ID].status = 'endInteraction';
  }
};
let resetAttempts = ID => {
  user[ID].attempts = 0;
};

//if not handling emoji responses, use this handler
let showEmojiResponse = ID => {
  showTyping(ID, 2200, function() {
    sendTextMessage(ID, 'Seems this old tech cannot decipher icons of emotion.', () => {
      showTyping(ID, 1000, function() {
        sendTextMessage(ID, 'Let’s try a simple yes or no.');
      });
    });
  });
};

// --------------- Methods -----------------------

/**
 * call triggered from Get Started button
 */
let initUser = ID => {
  user[ID] = {};
  user[ID].ID = ID;
  user[ID].first_name = 'Citizen';
  user[ID].attempts = 0;
  user[ID].guild = '';
  user[ID].preQuizFlag = true;
  user[ID].quizFlag = false;
};

//Facebook IDs to use
let adminUsers = [];

let inTestGroup = ID => {
  let compID = parseInt(ID);
  return adminUsers.indexOf(compID) > -1;
};

// The lambdaHandler function has a check for specific test users that can run the commands in this function
let adminCommandsCheck = (userID, userResponse) => {
  let followupInt = null;
  let followupTestID = 1673627912714016;

  switch (userResponse) {
    //USED TO RUN A FOLLOW UP MESSAGE TEST TO SPECIFIC USERS
    case 'followupimagetestdino':
    // followupTestID = 1768664439867579;
    case 'followupimagetestmatthew':
    // followupTestID = 1685460031507283;
    case 'followupimagetest':
      //
      return;
      break;

    default:
      return;
      break;
  }
};

//retrieve Facebook data
let getUserInfo = (ID, callback) => {
  callback = typeof callback !== 'undefined' ? callback : null;

  let url = `https://graph.facebook.com/${ID}?fields=first_name,last_name,profile_pic&access_token=${envInfo.pageAccessToken}`;
  request.get(url).end((err, res) => {
    let userinfo = {
      first_name: 'Citizen'
    };
    if (err) {
      if (callback !== null) {
        callback(userinfo);
      }
      return console.log(err);
    }
    userinfo = JSON.parse(res.text);
    if (callback !== null) {
      callback(userinfo);
    }
  });
};
let startSession = ID => {
  // console.log('startSession',ID);

  getUserInfo(ID, resp => {
    let userinfo = resp;
    setProfileInfo(ID, userinfo);
    user[ID].status = 'i1_scene1';
    user[ID].currentPhase = botScript;
    var data = {
      ID: ID,
      first_name: userinfo['first_name'],
      status: user[ID].status
    };
    // db.addUser(ID, data, () => {
    botHandler(ID, userinfo, false);
    // });
  });
};

let firstEntity = (entities, name) => {
  return entities && entities[name] && entities[name][0];
};

/**
 * Checks message for a yes or no response
 * @param  {} msgObj Message object sent back from Messenger
 * nlpPos && nlpNeg can be added to response to gauge confidence in the response
 */
let simpleYesNo = msgObj => {
  var testText = msgObj.message.text
    .toLowerCase()
    .replace(',', '')
    .replace('’', '');
  var hasYes =
    testText.includes('yes') ||
    yesResponses.indexOf(testText) > -1 ||
    (typeof msgObj.message.sticker_id !== 'undefined' && msgObj.message.sticker_id == 369239263222822);
  var hasNo = testText.includes('no') || noResponses.indexOf(testText) > -1;
  var mood = firstEntity(msgObj.message.nlp.entities, 'sentiment');
  var nlpPos = mood && mood.confidence > 0.5 && mood.value == 'positive';
  var nlpNeg = mood && mood.confidence > 0.5 && mood.value == 'negative';
  var response = hasYes && !hasNo ? 'yes' : hasNo && !hasYes ? 'no' : 'unknown';

  return response;
};

let expectedResponse = (userResponse, optionArray) => {
  return optionArray.indexOf(userResponse.toLowerCase()) > -1;
};

let pluralize = char => {
  return char[char.length - 1] == 's' ? char : char + 's';
};
/**
 * Set of checks we can do to gauge tone of user response. Will expand if needed.
 * @param  {} entities
 * @param  {} mood
 */
let checkMood = (entities, mood) => {
  /* RETURNS TONE OF USER RESPONSE */
  if (entities == {})
    return {
      result: null,
      found: false
    };

  var output = {
      result: null,
      found: true,
      verbal: ''
    },
    tmood;

  switch (mood) {
    case 'greeting':
      tmood = firstEntity(entities, mood);
      output.result = tmood && tmood.confidence > 0.5;
      output.verbal = output.result ? 'hello' : '';
      break;

    case 'isNeutral':
      tmood = firstEntity(entities, 'sentiment');
      output.result = tmood && tmood.confidence > 0.6 && tmood.value == 'neutral';
      break;

    case 'isHostile':
      tmood = firstEntity(entities, 'sentiment');
      output.result = tmood && tmood.confidence > 0.7 && tmood.value == 'negative';
      break;

    default:
      /* */
      break;
  }
  return output;
};

let updateQuizTally = response => {
  // answerTally[response] += 1;
  if (response.indexOf(',') > -1) {
    response.split(',').forEach(function(v, i) {
      answerTally[v] += 1;
    });
  } else {
    answerTally[response] += 1;
  }
};

/**
 * Primary handler for bot interaction
 * @param  {} data the message object that is returned from Facebook
 * @param  {} answerPending flag used to determine whether the handler should be expecting the answer to a question.
 */
let botHandler = (ID, data, answerPending) => {
  data = typeof data !== 'undefined' ? data : {};
  ID = typeof ID !== 'undefined' ? ID : userID;
  answerPending = typeof answerPending !== 'undefined' ? answerPending : false;
  let dbData = {};
  let callback = null;

  if (typeof user[ID] == 'undefined' || user[ID] == {}) {
    console.log('no status found');
    initUser(ID);
    // db.userFound(ID, {}, results => {
    user[ID].status = results.status;
    botHandler(ID, data, answerPending);
    // });
    return;
  }

  switch (user[ID].status) {
    // structure example
    case 'i1_scene1':
      // if (!answerPending) {

      // } else {
      // if (simpleYesNo(data) == 'yes' || simpleYesNo(data) == 'no') {

      // //use terniary if there is automatically a set botscript response
      // var resp = simpleYesNo(data) == 'yes' ? botScript.paths[user[ID].status].yes : botScript.paths[user[ID].status].no;
      // showTyping(ID, 5000, function() {
      //   sendTextMessage(ID, resp, () => {
      //     setTimeout(() => {
      //       changeStatus(ID);
      //     }, 5000);
      //   });
      // });
      // resetAttempts(ID);
      // } else {
      //   sendRejectionMessage(ID, true);
      // }
      // }
      break;

    case 'NoResponse':
      /* */
      break;

    default:
      /* */
      break;
  }
  dbData.status = user[ID].status;
  updateUserInfo(ID, dbData, callback);
};

// --------------- Main handler -----------------------

/**
 * Handles data returned from Messenger API
 * @param  {} event
 * @param  {} callback
 */

let lambdaHandler = (event, callback) => {
  var data = event.body;

  if (data.object === 'page') {
    data.entry.forEach(function(entry) {
      var pageID = entry.id;
      var timeOfEvent = entry.time;

      entry.messaging.forEach(function(msg) {
        //Sets userID for session
        userID = userID == null || msg.sender.id ? msg.sender.id : userID;
        userID = parseInt(userID);

        // CHECK FOR POSTBACK
        if (msg.postback) {
          /* CHECK FOR GET STARTED BUTTON CLICK */
          if (msg.postback.payload == 'GET_STARTED_PAYLOAD') {
            user[userID] = {};
            user[userID].ID = userID;
            user[userID].attempts = 0;
            user[userID].guild = msg.postback.referral !== undefined ? msg.postback.referral.ref : '???';
            user[userID].preQuizFlag = user[userID].guild == '???';
            user[userID].quizFlag = false;

            // db.userFound(userID, {}, () => {
            startSession(userID);
            // });
            // startSession(userID);
            return;
          }
          // console.log(`msg.postback ${JSON.stringify(msg.postback)}`);
          /* POSTBACK RESPONSE */
        } else if (typeof msg.message !== 'undefined' && typeof msg.message.quick_reply !== 'undefined') {
          /* QUICK REPLY (BUTTON) RESPONSE */
          msgObj = msg;
          btnFound = true;
          botHandler(userID, msg, true);
        } else if (typeof msg.message !== 'undefined' && !msg.message.is_echo) {
          /* MESSAGE HAS A EMOJI OR STICKER */
          if (typeof msg.message.sticker_id !== 'undefined') {
            if (msg.message.sticker_id == 369239263222822 && typeof user[userID] == 'undefined') {
              initUser(userID);
              // db.userFound(userID, {}, () => {
              startSession(userID);
              // });

              // } else if(msg.message.sticker_id == 369239263222822) {
              //   msg.message.text = 'yes';
              //   botHandler(userID, msg, true);
            } else {
              msg.message = 'okay';
              botHandler(userID, msg, true);
              // showEmojiResponse(userID);
            }
            return;
          }
          /* MESSAGE RESPONSE FROM USER */
          // let answerPresent = expectedResponse(
          //   msg.message.text.toLowerCase(),
          //   ['👍🏿', 'hello', 'hi', 'get started', 'greetings', 'hello?', 'start', 'anyone there?', '?']
          // );
          // if (answerPresent && (typeof user[userID] == 'undefined')) {
          //   // if ((typeof user[userID] == 'undefined')) {
          //   initUser(userID);
          //   db.userFound(userID, {}, () => {
          //     startSession(userID);
          //   });
          //   return;
          // }
          // console.log('user',userID);
          if (typeof user[userID] == 'undefined') {
            getUserInfo(userID, resp => {
              initUser(userID);
              let userinfo = resp;
              setProfileInfo(userID, userinfo);
              user[userID].status = 'i1_scene1';
              user[userID].currentPhase = phase1;
              var data = {
                ID: userID,
                first_name: userinfo['first_name'],
                status: 'i1_scene1'
              };

              // db.userFound(userID, {}, userCheck => {
              // if (typeof userCheck == 'not found') {
              db.addUser(userID, data, resp => {
                console.log('user added to db getting second interaction');
                console.log(resp);
                user[userID].status = 'i1_scene1';
                user[userID].currentPhase = botScript;
                botHandler(userID, msg, true);
              });
              /*   } else {
                  user[userID].status =
                    typeof userCheck.status !== 'undefined' &&
                    userCheck.status !== 'endInteraction1' &&
                    userCheck.status !== 'endInteraction2'
                      ? userCheck.status
                      : 'i4_scene1_followup';
                  user[userID].currentPhase =
                    parseInt(userCheck.phase) == 4 ? phase4 : parseInt(userCheck.phase) == 3 ? phase3 : botScript;
                  console.log('user session lost', user[userID].status);
                  botHandler(userID, msg, true);
                } */
              // });
            });
            return;
          }

          /* ADMIN COMMANDS */
          if (inTestGroup(userID)) {
            adminCommandsCheck(userID, msg.message.text.toLowerCase());
          }
          /* *************** */

          if (msg.message.sticker_id == 'status') {
            user[userID].currentStatus = user[userID].status;
            user[userID].status = 'status';
          }
          msgObj = msg;
          botHandler(userID, msg, true);
          // console.log('current Status: ' + user[userID].status);
        } else {
          // console.log("DATA RECEIVED: ", event);
        }
      });
    });
  }
  callback.sendStatus(200);

  // }
};

// ROUTING
config.initServer(lambdaHandler);
