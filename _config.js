'use strict';

var _createClass = (function() {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ('value' in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }
  return function(Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
})();

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true });
  } else {
    obj[key] = value;
  }
  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError('Cannot call a class as a function');
  }
}

const express = require('express');
const bodyParser = require('body-parser');
const https = require('https');
const fs = require('fs');

let botConfig = {
  pageAccessToken: '',
  appID: '',
  appSecret: '',
  verifyToken: '',
  privkey: '/privkey1.pem',
  cert: '/cert1.pem',
  chain: '/chain1.pem',
  pageID: '123456789012',
  database: {
    host: '',
    user: '',
    database: '',
    password: '',
    port: '3306'
  }
};

var config = (function() {
  function config() {
    _classCallCheck(this, config);
  }
  _createClass(config, [
    {
      key: 'configInfo',
      value: function configInfo() {
        return botConfig;
      }
    },
    {
      key: 'initServer',
      value: function initServer(handler) {
        const app = express().use(bodyParser.json());
        let pageToken = '';

        app
          .route('/webhook')
          .get((req, res) => {
            if (req.query['hub.verify_token'] === botConfig.verifyToken) {
              return res.send(req.query['hub.challenge']);
            }
            res.send('Error, wrong validation token');
          })
          .post((req, res) => {
            handler(req, res);
          });

        app.post('/token', (req, res) => {
          // console.log('token post', req.body);
          if (req.body.verifyToken === botConfig.verifyToken) {
            pageToken = req.body.token;
            return res.sendStatus(200);
          }
          res.sendStatus(403);
        });
        app.get('/token', (req, res) => {
          // console.log('token get', req.body);
          if (req.body.verifyToken === botConfig.verifyToken) {
            return res.send({
              token: pageToken
            });
          }
          res.sendStatus(403);
        });
        let server = require('http').createServer(app);
        server.listen(process.env.PORT || 55555, () => {
          console.log('App is ready on port 55555');
        });

        // https.createServer({
        //   key: fs.readFileSync(botConfig.privkey),
        //   cert: fs.readFileSync(botConfig.cert),
        //   ca: fs.readFileSync(botConfig.chain)
        // }, app).listen(55555, function () {
        //   console.log('App is ready on port 55555');
        // });
      }
    }
  ]);

  return config;
})();

exports.default = config;
module.exports = exports['default'];
